module.exports = function(app, passport, LocalStrategy) {

var empty;
var path = require('path');
var request = require('request');



passport.use(new LocalStrategy(
    function (username, password, done) {
    var apikey="Km7HfofC3xSY";
    var data={}

    data.username=username;
    data.password=password;
    data.apikey=apikey;

    console.log(data);

    request.post
    ({ 
       headers: {'content-type' : 'application/x-www-form-urlencoded'},
       url:'http://localhost:3010/auth/api/pinyin/login', 
       formData: data
       }, 
       function(err,httpResponse,body){ 

        console.log(body);

          if(body!=="{}"){
             var obj=JSON.parse(body);
             console.log(obj);
             var user={};
             user.username=obj.Pinyin_User.username;
             user.group=obj.Pinyin_User.group;
             
            return done(null, user);
          }
          else{
           return done(null, false);
          } 
      })    
  }
));



passport.serializeUser(function (user, done) {
  done(null, user);
});


passport.deserializeUser(function (username, done) {
  done(null, username);
});


app.get('/', function(req, res) {
  if(req.user==empty){
    console.log("user="+req.user);
  res.sendFile(path.join(__dirname, '../public/views', 'login.html'));}
  else{
    console.log("user="+req.user);
    res.sendFile(path.join(__dirname, '../public/views', 'index.html'));}
});



app.get('/me', function(req, res){

    
  if (req.user) {
        console.log(req.user);
    res.json(req.user);
  } else {
       
    res.sendStatus(204);
    //console.log(req.user);
  }
});



app.get('/login', function(req, res, next) {
  res.sendFile(path.join(__dirname, '../public/views', 'login.html'));
});


app.post('/login',
 passport.authenticate('local', {
 successRedirect: '/',
 failureRedirect: '/login?message_code=not_pass'
 })
);


app.get('/logout', function (req, res) {   	
 req.logout();
 res.json({ redirect: "/login" });     
});


app.post('/collection_data_oncms', function(req, res){

    var apikey="Km7HfofC3xSY";
    var data={}
  
    data.apikey=apikey;

    data.years_old=(req.body.years_old).toString();
    data.months_old=(req.body.months_old).toString();

    data.years_school=(req.body.years_school).toString();

    data.gender=req.body.gender;

    data.speak_home_or_not=req.body.speak_home_or_not;
    data.language_speak_at_home=req.body.language_speak_at_home;

    data.familyspeak_or_not=req.body.familyspeak_or_not;
    data.which_language_they_speak=req.body.which_language_they_speak;

    data.learntchinese_or_not=req.body.learntchinese_or_not;
    data.learntchinese_detail=req.body.learntchinese_detail;
    
    data.years_learnt=(req.body.years_learnt).toString();
    data.months_learnt=(req.body.months_learnt).toString();

    request.post({ 
     headers: {'content-type' : 'application/x-www-form-urlencoded'},
     url:'http://localhost:3010/auth/api/pinyin/collection_data',  
     formData: data
    }, 
    function(err,httpResponse,body){ 
         if(body!=="{}"){

            var obj=JSON.parse(body);
             console.log(obj);
             //var infoleft={};
             //notification.pushtime=obj.Educational_Platform_Notification[0].pushtime;
             //notification.pushmessage=obj.Educational_Platform_Notification[0].pushmessage;
             //infoleft.infomation=obj.Educational_Platform_Infoleft


          res.json({message:obj});
          }
          else{
          res.json({message:false});
          } 
      }) 

});




app.post('/get_group_oncms', function(req, res){

    var empty;
    var apikey="Km7HfofC3xSY";
    var data={}


    data.apikey=apikey;

    data.group=req.body.group;


    request.post({ 
     headers: {'content-type' : 'application/x-www-form-urlencoded'},
     url:'http://localhost:3010/auth/api/pinyin/get_group',   
     formData: data
    }, 
    function(err,httpResponse,body){ 
         if(body!=="{}"){

            var obj=JSON.parse(body);
             console.log(obj);
             var result={};
             result.data=obj;



          res.json({message:result});
          }
          else{
          res.json({message:false});
          } 
      }) 

});



};