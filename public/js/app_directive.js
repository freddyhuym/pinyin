var educational_app = angular.module('educational_app');



educational_app.directive('navBar', function() {
    return {
        controller: 'nav_bar_controller',
        templateUrl: '/views/nav_bar.html'
    };
});

educational_app.directive('startingContent', function() {
    return {
        controller: 'starting_content_controller',
        templateUrl: '/views/starting_content.html'
    };
});


educational_app.directive('collectionSurvey', function() {
    return {
        controller: 'collection_survey_controller',
        templateUrl: '/views/collection_survey.html'
    };
});


educational_app.directive('learningGroupa', function() {
    return {
        controller: 'learning_groupa_controller',
        templateUrl: '/views/learning_groupa.html'
    };
});


educational_app.directive('learningGroupb', function() {
    return {
        controller: 'learning_groupb_controller',
        templateUrl: '/views/learning_groupb.html'
    };
});


educational_app.directive('learningGroupc', function() {
    return {
        controller: 'learning_groupc_controller',
        templateUrl: '/views/learning_groupc.html'
    };
});


educational_app.directive('learningGroupd', function() {
    return {
        controller: 'learning_groupd_controller',
        templateUrl: '/views/learning_groupd.html'
    };
});


//--------------------------------------------------------------post




