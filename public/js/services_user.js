//建立userService module

angular.module('userService', [])
    .factory('$User', function($http ,$q) {
var userFactory;

return{

loadUser:function(){
var defer=$q.defer(); 

$http.get('/me').
            then(function(res) {
                userFactory = res.data.user;
                defer.resolve(userFactory);
                //return userFactory;
            }, function(error) {

                 userFactory = null;
                 defer.reject(userFactory);
                // return userFactory;
                   
           });

 return defer.promise;

}

/*
        var defer=$q.defer();  
        var empty;
        var userFactory = {};
        // 建立loadUser method
        userFactory.loadUser = function() {

                   
          //userFactory.userid=empty;
          //userFactory.status=empty;
            $http.
            get('/me').
            then(function(res) {
                userFactory.user = res.data.user;
                console.log("load User completely!");
                defer.resolve(userFactory);
                //return userFactory;
            }, function(error) {

                 userFactory.user = null;
                 defer.resolve(userFactory);
                // return userFactory;
                   
           });


        };
        // 呼叫method
        userFactory.loadUser();
        //return userFactory;
        return defer.promise;
*/




}


});



//建立userService module
/*
angular.module('userService', [])
    .factory('$User', function($http) {
        var userFactory = {};
        // 建立loadUser method
        userFactory.loadUser = function() {
            


            $http.
            get('/me').
            success(function(data) {
                userFactory.user = data.user;
                console.log("load User completely!");
            }).
            error(function(data, status) {
                //取得data
                //--錯誤回傳的資料{ error: 'User Not logged in!' }
                if (status === 401) {
                    userFactory.user = null;
                    console.log(data.error);
                }
            });



        };
        // 呼叫method
        userFactory.loadUser();
        return userFactory;
    });


*/