// Front-End
//建立routerRoutes
//dependencies: ngRoute

var educational_app = angular.module('routerRoutes', ['ngRoute']);


educational_app.config(function($routeProvider/*, $stateProvider*/) {
    $routeProvider
        .when('/', {
            template: '<starting-content></starting-content>'
        })
        .when('/collection_survey', {
            template: '<collection-survey></collection-survey>'
        })
        .when('/learning_groupa', {
            template: '<learning-groupa></learning-groupa>'
        })
        .when('/learning_groupb', {
            template: '<learning-groupb></learning-groupb>'
        })
       .when('/learning_groupc', {
            template: '<learning-groupc></learning-groupc>'
        })
        .when('/learning_groupd', {
            template: '<learning-groupd></learning-groupd>'
        })
     
     
        .otherwise({redirectTo:'/'});
        

        /*

        .when('/video', {
            template: '<video-page></video-page>'
        })
         .when('/_=_', {
            template: '<login-page></login-page>'
        })
        */
});






