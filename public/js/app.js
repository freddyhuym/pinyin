
'use strict';

var educational_app = angular.module('educational_app', 
    ["userService",
     "testuserService",
     "hidenavService",
     "routerRoutes",
     "ngSanitize",
     "com.2fdevs.videogular",
     "com.2fdevs.videogular.plugins.controls",
     "com.2fdevs.videogular.plugins.overlayplay",
     "com.2fdevs.videogular.plugins.buffering",
     "com.2fdevs.videogular.plugins.poster",

     "com.2fdevs.videogular.plugins.buffering",
     "com.2fdevs.videogular.plugins.imaads",
     'ui-notification',
     'textService'
     //"notificationTest"
     ]);






//--------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------




educational_app.controller('nav_bar_controller', function($scope, $http , $window, $User, $Hidenav, $testuser) {



 $scope.$watch(function () {
  return $Hidenav.nohide
}, function (newVal) {

console.log(newVal);
$scope.nohide=$Hidenav.nohide;

});
   




   $scope.logout = function(){
  
        $http.get('/logout').then(function(data){
        console.log(data);
	    console.log(data.data.redirect);
        $window.location.href = data.data.redirect; 
        });
    }

});




//--------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------



educational_app.controller('starting_content_controller', function($location, $scope, $http , $window, $User, $Hidenav, $testuser) {


  $scope.next=function(){


   $location.url("/collection_survey");
  }



});



educational_app.controller('collection_survey_controller', function($location, $scope, $http , $window, $User, $Hidenav, $testuser) {

var group;
var empty;

  $http.get('/me').then(function(data){

   console.log(data.data);
   group = data.data.group;
 

  })


  $scope.$watch('speak_home_or_not', function(value) {
      
      


    if(value=="speak_yes")
    { 
      
      $scope.speak_show="show";}
   
    else if(value=="speak_no")
    {  
      $scope.speak_show=empty; 
    }
    else
    {}
  
  });



    $scope.$watch('familyspeak_or_not', function(value) {
      
      


    if(value=="family_speak_yes")
    { 
      console.log(12);
      $scope.familyspeak_show="show";}
   
    else if(value=="family_speak_no")
    {  console.log(23);
      $scope.familyspeak_show=empty; 
    }
    else
    {}
  
  });




   $scope.$watch('learntchinese_or_not', function(value) {
      
      


    if(value=="learnt_chinese_yes")
    { 
      
      $scope.learntchinese_show="show";}
   
    else if(value=="learnt_chinese_no")
    {  
      $scope.learntchinese_show=empty; 
    }
    else
    {}
  
  });  


  $scope.$watch('language_speak_at_home', function(value) {
    
    console.log(value);
    $scope.language_speak_at_home=value;

  
  });

 $scope.$watch('which_language_they_speak', function(value) {
      
    $scope.which_language_they_speak=value;
  
  });  


  $scope.$watch('learntchinese_detail', function(value) {
      
    $scope.learntchinese_detail=value;
  
  });  


  $scope.next=function(){


    if($scope.years_old==empty)
    {
      alert("1.How old are you? (years)");
      return;
    }

    if($scope.months_old==empty)
    {
      alert("1.How old are you? (months)");
      return;
    }

    if($scope.years_school==empty)
    {
      alert("2.What year are you in at school?");
      return;
    }

    
    if($scope.gender==empty)
    {
      alert("3.What is your gender?");
      return;
    }


    if($scope.speak_home_or_not==empty)
    {
      alert("4.Do you speak language/languages other than English at home?");
      return;
    }

    if($scope.speak_home_or_not=="speak_yes" && $scope.language_speak_at_home==empty)
    {
      alert("4.Please specify the language/languages you speak at home");
      return;
    }


    if($scope.familyspeak_or_not==empty)
    {
      alert("5.Do members of your family speak languages other than English?");
      return;
    }

    if($scope.familyspeak_or_not=="family_speak_yes" && $scope.which_language_they_speak==empty)
    {
      alert("5.Which language/languages your family speak other than English?");
      return;
    }


    if($scope.learntchinese_or_not==empty)
    {
      alert("6.Have you learned Chinese in other schools? (For example, primary school/ community language school/Saturday school)?");
      return;
    }

    if($scope.learntchinese_or_not=="learnt_chinese_yes" && $scope.learntchinese_detail==empty)
    {
      alert("6.Please specify");
      return;
    }


    if($scope.years_learnt==empty)
    {
      alert("7.How long have you been learning Chinese? (years)");
      return;
    }

    if($scope.months_learnt==empty)
    {
      alert("7.How long have you been learning Chinese? (months)");
      return;
    }

     var item={}
     
     item.years_old=$scope.years_old
     item.months_old=$scope.months_old
      
     item.years_school=$scope.years_school
      
     item.gender=$scope.gender 

     item.speak_home_or_not=$scope.speak_home_or_not 
     if($scope.language_speak_at_home==empty)
     {item.language_speak_at_home=""}
     else
     {item.language_speak_at_home=$scope.language_speak_at_home}
          

     item.familyspeak_or_not=$scope.familyspeak_or_not 
     if($scope.which_language_they_speak==empty)
     {item.which_language_they_speak=""}
     else
     {item.which_language_they_speak=$scope.which_language_they_speak}

     item.learntchinese_or_not=$scope.learntchinese_or_not 
     if($scope.learntchinese_detail==empty)
     {item.learntchinese_detail=""}
     else
     {item.learntchinese_detail=$scope.learntchinese_detail}     
     
     item.years_learnt=$scope.years_learnt   
     item.months_learnt=$scope.months_learnt 
     

     $http.post('/collection_data_oncms',item).then(function(data){


      if(group == "a")
      {
        $location.url("/learning_groupa");
      }
      else if(group == "b")
      {
        $location.url("/learning_groupb");   
      }
      else if(group == "c")
      {
        $location.url("/learning_groupc");
      }
      else if(group == "d")
      {
        $location.url("/learning_groupd");
      }

      

     });


      console.log(item);

 
   //$location.url("/learning_groupa");
  }



});




educational_app.controller('learning_groupa_controller', function($timeout, $location, $scope, $http , $window, $User, $Hidenav, $testuser) {

    
   var mytimeout;
   var data;

    // $scope.play = function( item){
    //   var audio = new Audio(item);
    //  audio.play();
    // }





$scope.countdown=30;  
    

            





var item={};

item.group="a"
  

  $http.post('/get_group_oncms',item).then(function(data){

  
   data = data.data.message.data.message;
   var i=0;

   console.log(data);


    function play(item){
      var audio = new Audio(item);
     audio.play();
    }  



    $scope.start = function(){

      mytimeout = $timeout($scope.onTimeout,1000);
         $scope.photo=data[0].photo_path;
   console.log($scope.photo);
    
    }


    $scope.onTimeout = function(){

        
        $scope.countdown--
        mytimeout = $timeout($scope.onTimeout,1000);

        if( $scope.countdown==24)
        {
         play('/english_voices/'+data[i].english_voice_path)
        }

        if( $scope.countdown==18)
        {
         play('/chinese_voices/'+data[i].chinese_voice_path)
        }

        if( $scope.countdown==12)
        {
          $scope.text="Please read out loud the pinyin."
        } 


        if( $scope.countdown==0)
        {
           $scope.text="";

          if(i==data.length)
          {
            $timeout.cancel(mytimeout);
          }
          else
          {
            i=i+1;
            $scope.photo=data[i].photo_path;
            $scope.countdown=30
          }
          
          
         
        }

        if( $scope.countdown==-1)
        {
        
            $timeout.cancel(mytimeout);

    
          
          
         
        }




    };

        

    $scope.stop = function(){
        $timeout.cancel(mytimeout);
    };

    
    


    


  });








});



educational_app.controller('learning_groupb_controller', function($timeout,$location, $scope, $http , $window, $User, $Hidenav, $testuser) {

var item={};
  
item.group="b"
  

  $http.post('/get_group_oncms',item).then(function(data){

      console.log(data.data.message.data.message);
      

     });


});



educational_app.controller('learning_groupc_controller', function($timeout,$location, $scope, $http , $window, $User, $Hidenav, $testuser) {

var item={};
  item.group="c"
  

  $http.post('/get_group_oncms',item).then(function(data){

      console.log(data.data.message.data.message);
      

     });



});


educational_app.controller('learning_groupd_controller', function($timeout,$location, $scope, $http , $window, $User, $Hidenav, $testuser) {

  var mytimeout;
  var data;

  $scope.countdown=30;  



var item={};
  item.group="d"
  

  $http.post('/get_group_oncms',item).then(function(data){

        data = data.data.message.data.message;
   var i=0;

   console.log(data);


    function play(item){
      var audio = new Audio(item);
     audio.play();
    }  



    $scope.start = function(){

      mytimeout = $timeout($scope.onTimeout,1000);
         $scope.photo=data[0].photo_path;
   console.log($scope.photo);
    
    }


    $scope.onTimeout = function(){

        
        $scope.countdown--
        mytimeout = $timeout($scope.onTimeout,1000);

        if( $scope.countdown==24)
        {
         play('/english_voices/'+data[i].english_voice_path)
        }

        if( $scope.countdown==18)
        {
         play('/chinese_voices/'+data[i].chinese_voice_path)
        }

        if( $scope.countdown==12)
        {
          $scope.text="Please read out loud the pinyin."
        } 


        if( $scope.countdown==0)
        {
           $scope.text="";

          if(i==data.length)
          {
            $timeout.cancel(mytimeout);
          }
          else
          {
            i=i+1;
            $scope.photo=data[i].photo_path;
            $scope.countdown=30
          }
          
          
         
        }

        if( $scope.countdown==-1)
        {
        
            $timeout.cancel(mytimeout);

    
          
          
         
        }




    };

        

    $scope.stop = function(){
        $timeout.cancel(mytimeout);
    };
      

     });
  



});



