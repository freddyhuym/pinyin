var express = require('express');
var morgan = require('morgan');
var bodyParser = require('body-parser');
var session = require('express-session');
var passport = require('passport');
var LocalStrategy = require('passport-local').Strategy;





var app = express();






var port = process.env.PORT || 3011;




app.use(bodyParser.json());
app.use(bodyParser.urlencoded({
    extended: true
}));

app.use(session({
    secret: 'ilovekk',
    resave: false,
    saveUninitialized: false
}));

app.use(passport.initialize());
app.use(passport.session());




app.use(morgan('dev'));

// 設定路徑
require('./routes/api.js')(app, passport, LocalStrategy);

//讓此目錄下的html都可以作為static file
app.use(express.static(__dirname + '/public'));


//launch==================================================
app.listen(port);
console.log('Server is running on port ' + port + '..........');